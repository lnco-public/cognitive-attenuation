
***************************************************
BEHAVIORAL ANALYSIS

Script:
Behavioral_analysis_final.Rmd

N.B. Choose which analysis: pilot or main experiment

Main experiment behavioral files: 
Data_MRI_study.csv
Demographics_mri.csv

Pilot experiment behavioral files:
Data_pilot_study.csv
Demographics_pilot.csv

Control task of over verbal fluency:
Verbal_fluency_over_covert.csv

***************************************************
EXPERIMENTAL TASK

Folders:
Audio
Task
check the ReadMe file in the 'Task' folder for specific instructions.