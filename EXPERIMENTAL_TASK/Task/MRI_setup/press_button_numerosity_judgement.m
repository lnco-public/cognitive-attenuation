
function press_button_numerosity_judgement(PressJudg,PressTime,path)
%find the times, duration and number of button prsses during the numerosity judgement

for i = 1: length(PressJudg)
    press_trial = cell2mat(PressJudg(i));
    times_trial = cell2mat(PressTime(i));
    press_change = logical(diff(press_trial)); %the 0-1 logical change occurs when condition changes (marks beginning and ecnd of condition)
    press_change_filter = (find(press_change == 1));
    numb_presses = 0;
    numb_releases = 0;
    start_press_time = []; 
    stop_press_time = [];
    for ch = 1:length(press_change_filter)
        if press_trial(press_change_filter(ch)+1) == 1 
            %Press Starts
            numb_presses = numb_presses +1;
            start_press_time(numb_presses) = times_trial(press_change_filter(ch));
        elseif press_trial(press_change_filter(ch)+1) == 0
            %Press Stops
            numb_releases = numb_releases + 1; 
            stop_press_time(numb_releases) = times_trial(press_change_filter(ch));
        end     
    end
    if length(start_press_time) == length(stop_press_time)
        press_duration = stop_press_time - start_press_time;
    else    
        start_press_time = start_press_time(1:(length(start_press_time)-1));
        press_duration = stop_press_time - start_press_time;
    end
    start_press{i,1} = start_press_time;
    duration_press{i,1} = press_duration;
end
Length = 0;
Trial = 0;
count = 0;
LengthCond = [];
prev_cond = 0;
ButtonPressTime = start_press(:);
duration_peess_l = duration_press(:);
for p = 1:length(ButtonPressTime)
    Presses = cell2mat(ButtonPressTime(p,1));
    Presses = Presses';
    Duration = cell2mat(duration_peess_l(p,1));
    Duration = Duration';
    LengthP = length(Presses);
    Trial = Trial + 1;
    if Trial > 4
        Trial = 1;
        count = count+ 1;
        if count == 1            
            l_cond = repmat(count,(length(ButtonPress)-prev_cond),1);
        else 
            l_cond = repmat(count,(length(ButtonPress)-prev_cond),1);
            count = 0;
        end 
        prev_cond = length(ButtonPress);
        LengthCond = [LengthCond; l_cond];
    end 
    l_Trial = repmat(Trial,LengthP,1);   
    
    if p == 1
        ButtonPress(p:LengthP,1) = Presses;
        PressDuration(p:LengthP,1) = Duration;
        LengthTrial(p:LengthP,1) = l_Trial;
    else
        ButtonPress = [ButtonPress; Presses];
        PressDuration = [PressDuration; Duration];
        LengthTrial = [LengthTrial; l_Trial];
    end
    if p == 16 
        run = 1;
        run1 = repmat(run,(length(ButtonPress)),1);
    elseif p == 28 
        run = 2; 
        run2 = repmat(run,(length(ButtonPress)-length(run1)),1);
    elseif p == 40  
        run = 3;
        run3 = repmat(run,(length(ButtonPress)-(length(run2)+length(run1))),1);
    end    
end
RunsAll = [run1; run2; run3];
l_cond = repmat(2,(length(ButtonPress)-length(LengthCond)),1);
LengthCond = [LengthCond; l_cond];
ButtonPressJudg = [RunsAll LengthCond, LengthTrial, ButtonPress, PressDuration];
save([path 'ButtonPressJudgement.mat'], 'ButtonPressJudg'); 
csvwrite([path 'ButtonPressJudgement.csv'], ButtonPressJudg);
end