%Fix variable order when some run have crashed
%Add missing variables
length_trial = zeros(4,7);
time_trial_end = zeros(4,7);
time_trial_start = zeros(4,7);
%Load 1st task mat and add missing parts
load('Task.mat')
save('TaskN.mat', 'Accuracy', 'words_generated', 'numb_words_heard', 'target_words_range', 'word_judgement', 'confidence_percentage', 'reaction_time_judgement', 'reaction_time_confidence', 'letters_index', 'generation_time', 'start_generation_time', 'start_judgement_time', 'start_confidence_time', 'time_of_button_presses','time_between_button_presses', 'initial_judgement_number', 'start_instruction_time', 'time_beep_start', 'time_instr_eye_block', 'time_instr_eye_trial', 'triggerTime_start_runs', 'press_judg_time','press_judg_numb',  'word_played_duration', 'time_trial_start', 'time_trial_end', 'length_trial');
clear all
%make it structure
a1=load('TaskN.mat');
f=fieldnames(a1);
%load second task mat file and make it structure. Go to different folder
a2=load('Task.mat')

 for i = 1:length(f)
     for l = 1 : length(a1.(f{i}))
         if i == 24 
           a2.(f{i})(l,1)=  a1.(f{i})(l,1);
         else
           a2.(f{i})(:,l)=  a1.(f{i})(:,l);
         end
     end
 end
 
 save('Task.mat','-struct','a2')
 
 %after need to run the last part of the MRI_TI_setup_3runs_instscreen_cf script in order to
 %generate final files for data processing (also has to fing functions
 %needed in it ->change folder to the task script folder)