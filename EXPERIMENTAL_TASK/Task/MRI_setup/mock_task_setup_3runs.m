% Word numerosity judgement task, to run in MRI 
%Giedre 2017-2018  


% *******TASK DESCRIPTION******* 
% The task has two main conditions: self
% (subject generates silently as many words as possible in limited amout of
% time starting from the given cue letter. For each generated word press the
% button) and other (subject listens to the prerecorded list of words and
% have to notice the cued letter in the word. At the end of each trial (self
% or other)subject has to judge the number of words generated/heard. Before
% each trial subject hear short instruction (Ecoute or Pense). The task is
% performed with eyes closed, only when hear "BEEP" open the eyes. Task is
% balanced depending on the performance: depending on how many words were
% generated in 'self' condition, the same number of words will be played in
% 'other' condition. If no word were generated during 'self' condition 8
% word list will be played. Words are selected randomly but balancing for
% the cue letters in word. Cue letters are selected randomly but the same
% used for 'self' and 'other' conditions (in shuffled order) The break is
% inctroduced after 4 repetitions
%Waiting seconds for judgement and confidence 7s

% Clear the workspace
clearvars;
clc; 
clear all;  
sca;
rng shuffle;


%working directory
workdir = 'E:\TI_MRI_setup\';

%-------------------------
%	Set up subject number
%-------------------------

    nr_subject = input('Enter Subject Number:' ); 
    gender = input('Enter Subject Gender (m/f): ','s');
    ID = int2str(nr_subject);
    FileName=strcat('Subject_', ID); 
    
    path_subj = [workdir 'Subjects\' FileName];
    
    if exist(path_subj,'dir')
            disp('*********Subject already exists!*********')
            resp = input('Do you want to continue? (type: y/n): ', 's'); 
            if resp == 'n'
                return
            end    
    end
    
   mkdir (path_subj); %create folder for each subject

ListenChar(2);   
log = [path_subj '\log.txt'];  
diary (log)    
%Variables needed for the task
ITI = [4, 4.25, 4.5, 4.75]; %ITI
close_eyes = 2; %s
%conditions = {'self', 'other'}; %1 self,2 other
trials = 4; %same condition will be repeated
%repetitions = 4; % repetitions of each condition
pause = 15; %s, after 4 trials of each repetition (should be 15s)
n=2; %number of conditions
words_heard = [];
total_time = 7;
path = ([workdir 'Subjects\' FileName filesep]);

runs = [1 2 3];

%should be 10
cond_order = [1,2,1,2,1,2,1,2,1,2];

%-----------------------------------------------------------------
%	Generate letter list, which will be used for self and other conditions 
%-----------------------------------------------------------------

load([workdir 'Task\MRI_setup\words.mat']) %contains all words and letters and their indexes corresponding to file naming
letter_shfl = index_l(randperm(20)); % 16 letters needed; shuffle the order of letter indexes  length(index_l) 
letter_indx_self = reshape(letter_shfl,[5,4]); %4x4 matrix because will take 4 letters per one loop. Self and other will use the same letters

%other condition letters are shifted by 8 and shuffled in each rows columns
%(the cue letters are never repeated in the following block) 
shifted = circshift(letter_shfl, 8);
letter_indx_other = reshape(shifted,[5,4]); 
letter_indx_other = letter_indx_other(:, randperm(size(letter_indx_other,2)));


%orders letter arrays according to the cond_order in one matrix 10x4
i = 1;
ordered_conditions = zeros(10,4); %the matrix with letters for each condition
self = 1; other = 1;
while i <= length(cond_order)
    current_condition = cond_order(i);
   if mod(current_condition, 2) ~= 0
        ordered_conditions(i,:) = letter_indx_self(self,:);
        self = self + 1;
   elseif mod(current_condition, 2) == 0
        ordered_conditions(i,:) = letter_indx_other(other,:);
        other = other + 1;  
   end
    i = i + 1;
end

%-----------------------------------------------------------------
%	Audio settings 
%-----------------------------------------------------------------

% Number of channels and Frequency of the sound
nrchannels = 2;
freq = 44100;
Beep = psychwavread([workdir 'Audio\BEEP.wav']);
Beep = [Beep Beep];
Pense = psychwavread([workdir 'Audio\panse_' gender '.wav']);

%-----------------------------------------------------------------
%	Screen open 
%-----------------------------------------------------------------

% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);
% Get the screen numbers
screens = Screen('Screens');
% Draw to the external screen if avaliable
screenNumber = max(screens);
% Define black and white
white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
grey = white/2;
% Open an on screen window
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, black);
% Set the blend funciton for the screen
Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
% Sync us and get a time stamp
vbl = Screen('Flip', window);
waitframes = 1;
Screen('Flip',window); %swaps backbuffer to frontbuffer 
Screen('TextSize', window, 30);
% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', window); 
% Query the frame duration
ifi = Screen('GetFlipInterval', window);
% Get the centre coordinate of the window
[xCenter, yCenter] = RectCenter(windowRect);
% Maximum priority level
topPriorityLevel = MaxPriority(window);
Priority(topPriorityLevel);
instruction = 'Fermez les yeux jusqu''� entendre un bip';

%Keys settings for key presses 
KbName('UnifyKeyNames');
%upArrow = KbName('UpArrow'); 
ENTER = KbName('return');
Left = KbName('2@');
Right = KbName('3#');
Confirm = KbName('1!');
    
%-----------------------------------------------------------------
%	runs looping 
%-----------------------------------------------------------------

for run = 1:length(runs)
    if run == 1 
        cond_start = 1; 
        cond_stop = 4;
    elseif run == 2 
        cond_start = 5; 
        cond_stop = 7;
        DrawFormattedText(window, 'Pause','center', 'center', grey);
        Screen('Flip', window); 
        WaitSecs(20);
        Screen('Flip', window);
    elseif run == 3 
        cond_start = 8; 
        cond_stop = 10; 
        DrawFormattedText(window, 'Pause','center', 'center', grey);
        Screen('Flip', window); 
        WaitSecs(20);
        Screen('Flip', window);
    end 
    
    %-----------------------------------------------------------------
    %	Press the button to start the experiment 
    %-----------------------------------------------------------------

    disp('********************** Press ENTER to start! ***********************' ) 
    RestrictKeysForKbCheck([ENTER]);
    KbStrokeWait; 
    RestrictKeysForKbCheck([]);
    %-----------------------------------------------------------------
    %	Scanner trigger to start the experiment
    %-----------------------------------------------------------------
    disp('********************** Waiting for the scanner trigger ***********************' ) 
    %find key code for trigger key, which is 5 
    triggerCode = KbName('5%');
    keyIsDown = 0; 
    
    %Make sure no keys are disabled 
    DisableKeysForKbCheck([]); 
    
    %wait for the trigger 
    while 1 
        [ keyIsDown, pressedSecs, keyCode ] = KbCheck(-1);
        if keyIsDown
            if find(keyCode)==triggerCode
                break;
            end 
        end
    end 
    
    %Record trigger time for future reference 
    triggerTime_run = pressedSecs;
    fprintf('Trigger detected\n'); 
    
    %Disable 5 key for the rest of the script 
    DisableKeysForKbCheck([triggerCode]); 
    
    %-----------------------------------------------------------------
    %	Initialize sound 
    %-----------------------------------------------------------------
    InitializePsychSound; %flag (1) push for low latency
    % Open the default audio device [], with default mode [] (==Only playback),
    % and a required latencyclass of 1 == standard low-latency mode, as well as
    % a playback frequency of 'freq' and 'nrchannels' sound output channels.
    % This returns a handle 'pahandle' to the audio device:
    %pahandle = PsychPortAudio('Open', [], [], 1, freq, nrchannels, [], suggestedLatencySecs);
    % Try with the 'freq'uency we wanted:
    pahandle = PsychPortAudio('Open', [], [], 0, freq, nrchannels, [], []);
    %PsychPortAudio('UseSchedule', pahandle, 0);

    %-----------------------------------------------------------------
    %	Conditions looping 
    %-----------------------------------------------------------------

    for cond = cond_start:cond_stop 
        selected_cond = cond_order(cond);

        %-----------------------------------------------------------------
        %	close eyes instruction (rest period 15s)
        %-----------------------------------------------------------------   
        t_close_block = GetSecs-triggerTime_run;
        DrawFormattedText(window, [instruction],'center', 'center', grey);
        Screen('Flip', window); 
        WaitSecs(close_eyes);  
        Screen('Flip', window);
        WaitSecs(pause-2);

        if selected_cond == 1  
            %-----------------------------------------------------------------
            %	SELF
            %-----------------------------------------------------------------
            disp ('current selected condition is: SELF')
            t = [20; 25; 30; 35]; %time for trials (to generate words) [20; 25; 30; 35]
            t = t(randperm(length(t)));
            rest_t = ITI(randperm(length(ITI)));
            for trial = 1: trials 


                %-----------------------------------------------------------------
                %	Pause with close eyes instruction
                %-----------------------------------------------------------------
                t_close_trial = GetSecs-triggerTime_run;
                DrawFormattedText(window, [instruction],'center', 'center', grey);
                Screen('Flip', window);
                WaitSecs(close_eyes);     %introduce pause
                Screen('Flip', window);
                WaitSecs(rest_t(trial)-close_eyes); 

                %-----------------------------------------------------------------
                 %	one sound playing
                %-----------------------------------------------------------------
                letter_indx = ordered_conditions(cond,trial); %have to change every loop!!!!! take it from generated list
                % Read WAV letter file from filesystem:
                Letter = psychwavread ([workdir 'Audio\letters_' gender '\' 'letter' num2str(letter_indx) '.wav']); 
                PsychPortAudio('UseSchedule', pahandle, 0);
                instruction_t_start = GetSecs - triggerTime_run;
                Start = GetSecs;
                PsychPortAudio('FillBuffer', pahandle, Pense'); 
                PsychPortAudio('Start', pahandle, 1, 0, 1); 
                PsychPortAudio('Stop', pahandle, 1, 1);
                PsychPortAudio('FillBuffer', pahandle, Letter'); 
                PsychPortAudio('Start', pahandle, 1, 0, 1); 
                PsychPortAudio('Stop', pahandle, 1, 1);
                Stop = GetSecs-Start;
                WaitSecs(4-Stop);

                %Count key presses while waiting for the beep and generating
                %words 

                start_generation = GetSecs-triggerTime_run;

                % Set up the timer
                startTime = GetSecs;
                durationInSeconds = t(trial, 1);
                numberOfSecondsRemaining = durationInSeconds;
                key_press_self = 0;
                time_press = []; 
                time_between_words = [];
                while numberOfSecondsRemaining > 0 
                    numberOfSecondsElapsed = round(GetSecs - startTime);
                    numberOfSecondsRemaining = durationInSeconds - numberOfSecondsElapsed;
                    [ keyIsDown, seconds, keyCode ] = KbCheck;  
                    if keyCode(Confirm) == 1
                        key_press_self = key_press_self + 1; 
                        KbReleaseWait;
                        %calculating time when button press happened and
                        %between button presses. Statring with the first press
                        %from the indicated generation time
                        time_press(key_press_self) = seconds - triggerTime_run; 
                        time_between_words(1) = time_press(1)-start_generation;
                       if key_press_self >= 2
                          time_between_words(key_press_self) = time_press(key_press_self) - time_press(key_press_self-1);
                       end                    
                    end

                end
                %when generation has to end play beep
                t_beep = GetSecs-triggerTime_run;
                PsychPortAudio('FillBuffer', pahandle, Beep'); 
                PsychPortAudio('Start', pahandle, 1, 0, 1);
                % Stop playback:
                PsychPortAudio('Stop', pahandle, 1, 1);
                PsychPortAudio('DeleteBuffer');
                WaitSecs(1.5);
                
                %***************to do judgement***************
                start_judgement = GetSecs-triggerTime_run;
                [press_time, press, number, number_primary, RT] = numerosity_judgement(triggerTime_run, waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, Left, Right, total_time, selected_cond, window, windowRect, grey); %need to change just flip screen, not to close
                WaitSecs(total_time - RT);
                % Clear the screen
                Screen('Flip', window);
                %***************confidence***************
                WaitSecs(1);
                start_confidence = GetSecs-triggerTime_run;
                if number < 999 %999 is used as missed value to answer
                    [conf, RT_conf] = error_eval_mov(waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, total_time, number, window, windowRect, grey);
                    WaitSecs(total_time - RT_conf);
                    % Clear the screen
                    Screen('Flip', window);
                else 
                    disp('Skipping confidence judgement')
                    conf = 999;
                    RT_conf = 999;
                    WaitSecs(total_time);
                end    

                %Accuracy 
                acc = number - key_press_self;

                %***************OUTPUT***************
                number_primary_judgement (trial, 1) = number_primary;
                t_button_press{trial, 1} = time_press;
                t_between_press{trial, 1} = time_between_words;
                accuracy(trial, 1) = acc;
                generated(trial, 1) = key_press_self;
                answer_judgement(trial, 1) = number; 
                answer_confidence (trial, 1) = conf;
                response_time_judgement (trial, 1) = RT; 
                response_time_confidence (trial, 1) = RT_conf;
                Letter_played(trial, 1) = letter_indx;
                t_instr_start(trial,1) = instruction_t_start;
                t_gen_start (trial, 1) = start_generation; 
                t_judg_start (trial, 1) = start_judgement;
                t_conf_start (trial, 1) = start_confidence;
                t_close_start (trial, 1) = t_close_trial;
                t_beep_start (trial,1) = t_beep;
                t_judg_presses{trial,1} = press_time; 
                num_judg_presses{trial,1} = press;
            end    
        else
            %-----------------------------------------------------------------
            %	OTHER
            %-----------------------------------------------------------------
            disp ('current selected condition is: OTHER')
            %the order for the word range selection and times, schuffled for
            %each mini block
            order = [1 2 3 4];
            order = order(randperm(length(order)));
            rest_t = ITI(randperm(length(ITI)));
            generated_self = generated;
            t_between_press_self = t_between_press;

           for trial = 1: trials 

                %-----------------------------------------------------------------
                %	Pause with close eyes instruction
                %-----------------------------------------------------------------
                t_close_trial = GetSecs-triggerTime_run;
                DrawFormattedText(window, [instruction],'center', 'center', grey);
                Screen('Flip', window);
                WaitSecs(close_eyes);     %introduce pause
                Screen('Flip', window);
                WaitSecs(rest_t(trial)-close_eyes); 

               %word range and indexes to be used
               num = order(trial); 
               times_between = t_between_press_self{num}; %taken from self condition
               range = generated_self(num); %the number of words to be played  
               if range < 6 %(<6)to make sure that sequence plays despite no previous answer in 'self' condition 
                times_between = randi([2,4],1,9);   
                range = 9; 
               end 
               letter_indx = ordered_conditions(cond,trial);  
               %function which generates list of words to be played including
               %controled amount of cued words
               [word_in, selected_cues] = word_list(range, letter_indx, letter, word, index_w);
               instruction_t_start = GetSecs - triggerTime_run;
               start_generation = GetSecs - triggerTime_run + 4; %because 4 seconds are for instruction and cue letter

               %listen to the pre-recorded sound
               [key_press_other, time_press, time_between_words, S, Total_times] = loop_sounds(pahandle, Confirm, gender, triggerTime_run, times_between, start_generation, word_in, letter_indx, workdir);  %call function to play selected sounds (including instruction, cue letter and beep at the end

               duration_played = S - 4.5; %exclude time of the instruction, cue letter and beep
               t_beep = instruction_t_start + S - 0.5;
               %WaitSecs(1);
               
               %***************to do judgement*************** 
               start_judgement = GetSecs-triggerTime_run;
               [press_time, press, number, number_primary, RT] = numerosity_judgement(triggerTime_run, waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, Left, Right, total_time, selected_cond, window, windowRect, grey); %need to change just flip screen, not to close
               WaitSecs(total_time - RT);
               WaitSecs(1); 
               % Clear the screen
               Screen('Flip', window);
               %***************confidence*************** 
               start_confidence = GetSecs-triggerTime_run;
               if number < 999
                    [conf, RT_conf] = error_eval_mov(waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, total_time, number, window, windowRect, grey);
                    WaitSecs(total_time - RT_conf);
                    % Clear the screen
                    Screen('Flip', window);
               else 
                    disp('Skipping confidence judgement') 
                    conf = 999;
                    RT_conf = 999;
                    WaitSecs(total_time);
               end

               %Accuracy 
               acc = number - range;

               %***************OUTPUT***************
               number_primary_judgement (trial, 1) = number_primary;
               target_words (trial, 1) = selected_cues;
               t (trial, 1) = duration_played;
               each_audio_duration {trial,1} = Total_times;
               t_button_press{trial, 1} = time_press;
               t_between_press{trial, 1} = time_between_words;
               accuracy(trial, 1) = acc;
               answer_judgement(trial, 1) = number;
               answer_confidence (trial, 1) = conf;
               response_time_judgement (trial, 1) = RT;
               response_time_confidence (trial, 1) = RT_conf;
               generated(trial, 1) = key_press_other;
               words_heard(trial, 1) = range; 
               Letter_played(trial, 1) = letter_indx;
               t_instr_start(trial,1) = instruction_t_start;
               t_gen_start (trial, 1) = start_generation; 
               t_judg_start (trial, 1) =  start_judgement;
               t_conf_start (trial, 1) = start_confidence;
               t_close_start (trial, 1) = t_close_trial;
               t_beep_start (trial,1) = t_beep;
               t_judg_presses {trial,1} = press_time; 
               num_judg_presses {trial,1} = press;
               
           end 

        end
        %to save the output NEED TO SELECT MORE OUTPUT VARIABLES (LETTER, TIME,
        %WORDS?)
        initial_judgement_number (:,cond) = number_primary_judgement;
        Accuracy(:,cond) = accuracy;
        words_generated(:,cond) = generated;  %words generated and heard cue letters in words
        if selected_cond == 2
            numb_words_heard(:,cond) = words_heard;
            target_words_range (:,cond) = target_words;
            word_played_duration (:,cond) = each_audio_duration;
        end
        word_judgement(:,cond) = answer_judgement; 
        confidence_percentage(:,cond) = answer_confidence;
        reaction_time_judgement(:,cond) = response_time_judgement; 
        reaction_time_confidence(:,cond) = response_time_confidence;
        letters_index (:,cond) = Letter_played;
        generation_time(:,cond) = t; 
        start_instruction_time(:,cond) = t_instr_start;
        start_generation_time(:,cond) = t_gen_start;
        start_judgement_time(:,cond) = t_judg_start;
        start_confidence_time(:,cond) = t_conf_start; 
        time_of_button_presses(:,cond) = t_button_press;
        time_between_button_presses(:,cond) = t_between_press;
        time_instr_eye_trial (:,cond) = t_close_start;
        time_instr_eye_block (:,cond) = t_close_block;
        time_beep_start (:,cond) = t_beep_start;
        press_judg_time (:,cond) = t_judg_presses; 
        press_judg_numb (:,cond)=  num_judg_presses;
    end
    % Close the audio device:
    PsychPortAudio('Close', pahandle);
    triggerTime_start_runs(run,1) = triggerTime_run;
    save([path 'Task.mat'], 'Accuracy', 'words_generated', 'numb_words_heard', 'target_words_range', 'word_judgement', 'confidence_percentage', 'reaction_time_judgement', 'reaction_time_confidence', 'letters_index', 'generation_time', 'start_generation_time', 'start_judgement_time', 'start_confidence_time', 'time_of_button_presses','time_between_button_presses', 'initial_judgement_number', 'start_instruction_time', 'time_beep_start', 'time_instr_eye_block', 'time_instr_eye_trial', 'triggerTime_start_runs', 'press_judg_time','press_judg_numb',  'word_played_duration');
    Run_time = (GetSecs - triggerTime_run)/60;
    fprintf(['\n****************\n' 'Time of run' num2str(run) ' is: ' num2str(Run_time) ' minutes\n****************\n']);
end
ListenChar(1); 
save([path 'Task.mat'], 'Accuracy', 'words_generated', 'numb_words_heard', 'target_words_range', 'word_judgement', 'confidence_percentage', 'reaction_time_judgement', 'reaction_time_confidence', 'letters_index', 'generation_time', 'start_generation_time', 'start_judgement_time', 'start_confidence_time', 'time_of_button_presses','time_between_button_presses', 'initial_judgement_number', 'start_instruction_time', 'time_beep_start', 'time_instr_eye_block', 'time_instr_eye_trial', 'triggerTime_start_runs', 'press_judg_time','press_judg_numb',  'word_played_duration');
tEnd = GetSecs; 
Exp_time = (tEnd - triggerTime_start_runs(1))/60; %time in minutes 
fprintf(['\n****************\n' 'Total experiment time was: ' num2str(Exp_time) ' minutes\n']);
DrawFormattedText(window, 'Fin de l''exp�rience!','center', 'center', grey);
Screen('Flip', window);
WaitSecs(5) 
sca 
%-----------------------------------------------------------------
%	Data long format saving
%-----------------------------------------------------------------
Subject = repmat(nr_subject,40,1);
Conditions = [1; 1; 1; 1; 2; 2; 2; 2];  
Conditions_all = repmat(Conditions,5,1); 
Run1 = repmat(runs(1),16,1); 
Run2 = repmat(runs(2),12,1);
Run3 = repmat(runs(3),12,1);
Runs = [Run1; Run2; Run3];
A = Accuracy(:);
WordsGenerated = words_generated(:);
WordsHeardNumber = numb_words_heard(:);
NumerosityJudgement = word_judgement(:);
ErrorJudgement = confidence_percentage(:);
RTNumerosityJudgement = reaction_time_judgement(:);
RTErrorJudgement = reaction_time_confidence(:);
LetterIndex = letters_index(:); 
WordGenerationTime = generation_time (:);
TargetWordRange = target_words_range(:);
InitialJudgementScaleValue = initial_judgement_number (:);
DataOutput = [Subject, Runs, Conditions_all, A, WordsGenerated, WordsHeardNumber, NumerosityJudgement, ErrorJudgement, RTNumerosityJudgement, RTErrorJudgement, WordGenerationTime, LetterIndex, TargetWordRange, InitialJudgementScaleValue]; 
save([path 'DataOutput.mat'], 'DataOutput'); 
csvwrite([path 'DataOutput.csv'],DataOutput);
TableData = table(Subject, Runs, Conditions_all, A, WordsGenerated, WordsHeardNumber, NumerosityJudgement, ErrorJudgement, RTNumerosityJudgement, RTErrorJudgement, WordGenerationTime, LetterIndex, TargetWordRange, InitialJudgementScaleValue);
writetable(TableData, [path 'TableData.csv']);
EyesCloseTrialTime =  time_instr_eye_trial(:);
EyesCloseBlockTime =  time_instr_eye_block(:);
csvwrite([path 'EyesCloseBlockTime.csv'],EyesCloseBlockTime);
BeepTime = time_beep_start(:);
StartInstructionTime = start_instruction_time(:);
StartGenerationTime = start_generation_time(:);
StartNumerosityJudgementTime = start_judgement_time(:);
StartErrorJudgementTime = start_confidence_time(:);
TimesConditions = [Subject, Runs, Conditions_all, StartInstructionTime, StartGenerationTime, WordGenerationTime, StartNumerosityJudgementTime, StartErrorJudgementTime, RTNumerosityJudgement, RTErrorJudgement, EyesCloseTrialTime, BeepTime];
save([path 'TimesConditions.mat'], 'TimesConditions');
TableTimes = table(Subject, Runs, Conditions_all, StartInstructionTime, StartGenerationTime, WordGenerationTime, StartNumerosityJudgementTime, StartErrorJudgementTime, RTNumerosityJudgement, RTErrorJudgement, EyesCloseTrialTime, BeepTime);
writetable(TableTimes, [path 'TableTimes.csv']);
csvwrite([path 'TimesConditions.csv'], TimesConditions);

%save button presses 
Length = 0;
Trial = 0;
count = 0;
LengthCond = [];
prev_cond = 0;
ButtonPressTime = time_of_button_presses(:); %button presses durin the word generation
PressJudg = press_judg_numb(:); %button presses during numerosity judgement
PressTime = press_judg_time(:); %button presses during numerosity judgement
press_button_numerosity_judgement(PressJudg,PressTime,path)
for p = 1:length(ButtonPressTime)
    Presses = cell2mat(ButtonPressTime(p,1));
    Presses = Presses'; 
    LengthP = length(Presses);
    Trial = Trial + 1;
    if Trial > 4
        Trial = 1;
        count = count+ 1;
        if count == 1            
            l_cond = repmat(count,(length(ButtonPress)-prev_cond),1);
        else 
            l_cond = repmat(count,(length(ButtonPress)-prev_cond),1);
            count = 0;
        end 
        prev_cond = length(ButtonPress);
        LengthCond = [LengthCond; l_cond];
    end 
    l_Trial = repmat(Trial,LengthP,1);   
    
    if p == 1
        ButtonPress(p:LengthP,1) = Presses;
        LengthTrial(p:LengthP,1) = l_Trial;
    else
        ButtonPress = [ButtonPress; Presses];
        LengthTrial = [LengthTrial; l_Trial];
    end
    if p == 16 
        run = 1;
        run1 = repmat(run,(length(ButtonPress)),1);
    elseif p == 28 
        run = 2; 
        run2 = repmat(run,(length(ButtonPress)-length(run1)),1);
    elseif p == 40  
        run = 3;
        run3 = repmat(run,(length(ButtonPress)-(length(run2)+length(run1))),1);
    end    
end 
RunsAll = [run1; run2; run3];
l_cond = repmat(2,(length(ButtonPress)-length(LengthCond)),1);
LengthCond = [LengthCond; l_cond];
ButtonPressAll = [RunsAll LengthCond, LengthTrial, ButtonPress];
save([path 'ButtonPressAll.mat'], 'ButtonPressAll'); 
csvwrite([path 'ButtonPressAll.csv'], ButtonPressAll);
diary off