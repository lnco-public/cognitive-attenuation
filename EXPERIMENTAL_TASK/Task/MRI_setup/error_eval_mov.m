function [conf, RT_conf] = error_eval_mov(waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, total_time, number, window, windowRect, grey)

%-----------------------------------------------------------------
%	The inputs needed if want to try function separately (examples)
%-----------------------------------------------------------------
% Clear the workspace and the screen
% sca;
% close all;
% clearvars;
% 
% %number - judgement done before. need to be displaid for comparison 
% 
% number = 10;
% 
% % Here we call some default settings for setting up Psychtoolbox
% PsychDefaultSetup(2);
% 
% % Get the screen numbers
% screens = Screen('Screens');
% 
% % Draw to the external screen if avaliable
% screenNumber = max(screens);
% 
% % Define black and white
% white = WhiteIndex(screenNumber);
% black = BlackIndex(screenNumber);
% grey = white/2;
% % Open an on screen window
% [window, windowRect] = PsychImaging('OpenWindow', screenNumber, black); 
% %----------------------------------------------------------------- 
%---------------
% Screen Setup (error judgement)
%--------------- 





%Error scale number 
error_number = 0;

%increase every step

increase = (screenXpixels*0.5)/10; %partition of steps

%Primary width is randomized 
order = randi([1,5],1);
primary_width = 20; %primary width
possible_width = [primary_width (primary_width+increase*2) (primary_width+increase*4) (primary_width+increase*6) (primary_width+increase*8)];
width = possible_width(order);

%Rectangle features
baseRect = [0 0 width 150]; %moving rectangle
statRect = [0 0 20 200]; %stationary rectangle
    
% Set the color of the rect
transparecy = 1;
ch_trans = 0.1;
possible_transparecy = [transparecy transparecy-ch_trans transparecy-ch_trans*2 transparecy-ch_trans*3 transparecy-ch_trans*4];
transparecy = possible_transparecy(order);

% Set the intial position of the square to be in the centre of the screen

squareX = screenXpixels*0.5;
squareY = screenYpixels*0.5;

% This is the cue which determines whether we exit the demo
exitDemo = false;
%Get primary time
t0   = GetSecs;

%Primary settings for movement 
time1 = 0;
time2 = 0; 
time = 0;
rep_pos = [0 1];
rep_pos = rep_pos(randperm(length(rep_pos)));
rep = rep_pos(1);

%time steps of increase (in seconds)
t = 0.5;

% % Sync us and get a time stamp
vbl = Screen('Flip', window);
% waitframes = 1;

% % Maximum priority level
% topPriorityLevel = MaxPriority(window);
% Priority(topPriorityLevel);

% Loop the animation until the escape key is pressed 
while exitDemo == false

    % Check the keyboard to see if a button has been pressed
    [keyIsDown,secs, keyCode] = KbCheck;

%*******************Moving rectangle in time intervals of t*****************

    if round((time1 + t),2) == round(time2,2)
       time = t;
       time1 = time2;
    end    
     if keyCode(Confirm)
         exitDemo = true; 
         disp('ERROR ANSWER WAS GIVEN')
     elseif (rep == 0 && time == t)
        width = width + increase*2; %increment of rectangle width
        error_number = round(((width-20)/increase)/2); %up to 8
        pos_err_post = ((screenXpixels*0.5 + (width/2))- 30); %coordinates to draw rectangles from the center both directions equally
        pos_err_neg = ((screenXpixels*0.5 - (width/2))-30); 
        transparecy = transparecy - 0.1; %rectangle intensity of color
        time = 0;
   
     elseif (rep == 1 && time == t)  
        width = width - increase*2;
        error_number = round(((width-20)/increase)/2); 
        pos_err_post = ((screenXpixels*0.5 + (width/2))- 30);
        pos_err_neg = ((screenXpixels*0.5 - (width/2))-30);
        transparecy = transparecy + 0.1;
        time = 0;
     end
     
     %Set the bound for the rectangle size in order not to violate the
     %preffere size of the used screen
     if width < 20 
         width = 20; 
         error_number = 0;
         transparecy = 1;
         rep = 0;
     elseif width > screenXpixels*0.5  
         width = screenXpixels*0.5;
         error_number = round(((width-20)/increase)/2);
         pos_err_post = (screenXpixels*0.5 + (width/2)-30);
         pos_err_neg = (screenXpixels*0.5 - (width/2)-30); 
         transparecy = 0.5; 
         rep = 1;
     end    
    
    rectColor = [0.5 0.5 0.5 transparecy];
    baseRect = [0 0 width 150]; %changing size rectangle
    % Center the rectangle on the centre of the screen; will change size every t seconds automatically 
    centeredRect = CenterRectOnPointd(baseRect, squareX, squareY);


    %*******************Stationary rectangles*****************
    statRect = CenterRectOnPointd(statRect, screenXpixels*0.5, screenYpixels*0.5);    
    
    % Draw the rect to the screen
    Screen('FillRect', window, grey, statRect); 
    Screen('FillRect', window, rectColor, centeredRect);
    
   
    % Draw all the text in one go
    %Screen('TextSize', window, 50);
    line1 = ['Indiquez votre marge d''erreurs? \n\n Votre r�ponse �tait:\n\n' num2str(number) ' mots'];
    
    DrawFormattedText(window, [line1],'center', ((screenYpixels * 0.25)-50), grey);
    DrawFormattedText(window, '+/- mots','center', (screenYpixels * 0.75), grey);
    if error_number == 0
    DrawFormattedText(window, '0', ((screenXpixels * 0.5)- 15), ((screenYpixels * 0.5)+200), grey); 
    elseif error_number > 0 
       post = ['+' num2str(error_number)];
       neg = ['-' num2str(error_number)];
       DrawFormattedText(window, [post], (pos_err_post), ((screenYpixels * 0.5)+200), grey);  
       DrawFormattedText(window, [neg], (pos_err_neg), ((screenYpixels * 0.5)+200), grey); 
    end   
    vbl  = Screen('Flip', window, vbl + (waitframes - 0.5) * ifi);
    %Time
    
    time2 = round((time2 + ifi),2);
    
    conf = error_number;
     
    %time2 =GetSecs; 
    if time2 >= total_time
       exitDemo = true; 
       disp('DID NOT MAKE ERROR JUDGEMENT ON TIME')
       conf = 999; 
    end   
       
end
%Get RT (how long took to answer)
%seconds = GetSecs;
RT_conf = time2;
% % Clear the screen
% Screen('Flip', window);
end