
function [word_in, selected_cues] = word_list(range, letter_indx, letter, word, index_w)
    %This function generates list of word indexes to be played according to the
    %given range and part of words (range/2) will have the cue letter (input:
    %letter_indx). The list will be changed each trial. 
    %Input needed: range(selected_range, letter_indx, letter, word, index_w
%     range = 6; 
%     letter_indx = 0;
    jitter = randi([0,2],1);
    selected_cues = round(range/2)+jitter;
    
    %the real letter selected (matches with the file numbering)
    selected_letter = letter((letter_indx+1),1);  
    %find cue words-all of them
    %for matlab 2016
%     word_list_cues_all = find(contains(word,selected_letter)); %just change, added _all
%     %find logical array where words start with cue letter
%     first_letter_words = startsWith(word,selected_letter);
    %for matlab 2014
    cells_list = strfind(word,selected_letter);
    word_list_cues_all = find(~cellfun(@isempty,cells_list));
    first_letter_words = strncmpi(word, selected_letter,1);
     
    word_new = word;
    %remove the words from list starting with cue letter 
    for i = 1:length(word)
        if first_letter_words(i) == 1
            word_new{i} = 'xxx';
        end        
    end 
    %indexes with words with cue letter but do not start as first letter
%     word_list_cues = find(contains(word_new,selected_letter));
    cells_list_rm = strfind(word_new,selected_letter);
    word_list_cues = find(~cellfun(@isempty,cells_list_rm));
    %select random cue words according to cue range
    size=numel(word_list_cues);
    size_all = numel (word_list_cues_all);
    %this checks that cue letter has enough possible words. If not enough
    %takes all cue words even starting from cue letter! Still might be a
    %problem!!! if a lot of words generated. Letter j has only 6 possible
    %words!!!!!!!!
    if size < selected_cues
        disp('NOT ENOUGH CUE WORDS, TAKING EXTRA WORDS STARTING FROM CUE LETTER AND REDUCING CUE RANGE')
        disp ('Existing number of all cue words')
        disp(word_list_cues_all)
        disp(selected_letter)
        selected_cues = round(range/2);
        if size_all < selected_cues 
           selected_cues = 2;
           index_word_list_cues= word_list_cues_all(randperm(size_all,selected_cues)); 
           disp('CUE WORDS ARE REDUCED TILL 2!')
        end   
        index_word_list_cues= word_list_cues_all(randperm(size_all,selected_cues)); 

  
    else
        index_word_list_cues= word_list_cues(randperm(size,selected_cues)); 
    end  
    
    for c=1:numel(index_word_list_cues)
         word_cues_final(c,1) = index_w((index_word_list_cues(c,1)),1);
    end
    %remove cue words from word list, all cue words
    cleaned_index_w = [];
    cleaned_index_w = index_w;
    for all=1:numel(word_list_cues_all)
        cleaned_index_w(word_list_cues_all(all))=[999];%not aplicable words
    end
    %randomly select words needed for missing range part (no cue letter containing)
    cleaned_index_w(cleaned_index_w == 999)=[];
    size1=numel(cleaned_index_w); 
    word_other_final=cleaned_index_w(randperm(size1,(range-selected_cues))); 
    %combine cued word list with left; shuffle 
    word_list_final = [word_cues_final;word_other_final];
    word_in = word_list_final(randperm(length(word_list_final))); 
    clearvars word_cues_final word_other_final 
end