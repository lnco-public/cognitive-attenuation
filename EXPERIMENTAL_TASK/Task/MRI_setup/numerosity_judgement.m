function [press_time, press, number, number_primary, RT] = numerosity_judgement(triggerTime_run, waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, Left, Right, total_time, selected_cond, window, windowRect, grey)
%---------------
% Screen Setup (numerosity judgement)
%--------------- 
%Clear the workspace and the screen
% sca;
% %close all;
% %clearvars;
% 
% % Here we call some default settings for setting up Psychtoolbox
% PsychDefaultSetup(2);
% 
% % Get the screen numbers
% screens = Screen('Screens');
% 
% % Draw to the external screen if avaliable
% screenNumber = max(screens);
% 
% % Define black and white
% white = WhiteIndex(screenNumber);
% black = BlackIndex(screenNumber);
% % Open an on screen window
% [window, windowRect] = PsychImaging('OpenWindow', screenNumber, black); 

% Set the blend funciton for the screen
%Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% % Get the size of the on screen window
% [screenXpixels, screenYpixels] = Screen('WindowSize', window); 
% 
% % Query the frame duration
% ifi = Screen('GetFlipInterval', window);
% 
% % Get the centre coordinate of the window
% [xCenter, yCenter] = RectCenter(windowRect);
 % Sync us and get a time stamp
vbl = Screen('Flip', window);

%Rectangle features which are not used (still needed but not drawn)
% Make a base Rect of 200 by 200 pixels
baseRect = [0 0 5 5];
% Set the color of the rect 
%rectColor = [1 1 1];

% Set the intial position of the square to be in the centre of the screen
%The coordinate X is randomly selected (always starts in different postion)
init_coord = randi([0 screenXpixels],1,1);
squareX = init_coord;
squareY = yCenter;

% Set the amount we want our square to move on each button press
pixelsPerPress = 10;

% % Sync us and get a time stamp
% vbl = Screen('Flip', window);
% waitframes = 1;

% Maximum priority level
% topPriorityLevel = MaxPriority(window);
% Priority(topPriorityLevel);

% This is the cue which determines whether we exit the demo
exitDemo = false;
%Get primary time
t0   = GetSecs;
time = 0; 
number_primary = round(squareX / 96);
press = 0;
press_time = [];

% Loop the animation until the escape key is pressed
while exitDemo == false

    % Check the keyboard to see if a button has been pressed
    [keyIsDown,secs, keyCode] = KbCheck;

    % Depending on the button press, either move ths position of the square
    % or exit the demo
    if keyCode(Confirm)
        exitDemo = true;
        disp('NUNMEROSITY ANSWER WAS GIVEN')
    elseif keyCode(Left)
        %count times of button presses
        %change position
        squareX = squareX - pixelsPerPress;
    elseif keyCode(Right)
        squareX = squareX + pixelsPerPress;
    end
   press(length(press)+1) = keyIsDown;       
   press_time(length(press)) = secs - triggerTime_run;
    % We set bounds to make sure our square doesn't go completely off of
    % the screen
    if squareX < 0
        squareX = 0;
    elseif squareX > screenXpixels
        squareX = screenXpixels;
    end
    


    % Center the rectangle on the centre of the screen (X coordinate is
    % randomly selected)
    centeredRect = CenterRectOnPointd(baseRect, squareX, squareY);

    % Draw the rect to the screen
    %Screen('FillRect', window, grey, centeredRect);
    
    %Define the number according to the coordinate x (squareX)
    number = round(squareX / 96); %the number is dependent on the screen size!!!!!!
    
    
    % Draw all the text in one go
    %Screen('TextSize', window, 50);
    if selected_cond == 1
        line1 = ['\n\n\n Nombre de mots que j''ai g�n�r�: \n\n\n\n' num2str(number)];
    elseif selected_cond == 2
        line1 = ['\n\n\n Total nombre de mots que j''ai entendu: \n\n\n\n' num2str(number)];
    end    
    DrawFormattedText(window, [line1],...
    'center', screenYpixels * 0.25, grey);

    % Flip to the screen
    vbl  = Screen('Flip', window, vbl + (waitframes - 0.5) * ifi);
    
    %Time
    time = time + ifi;
    
    if time >= total_time
        exitDemo = true;
        disp('DID NOT MAKE NUMEROSITY JUDGEMENT ON TIME')
        number = 999;
    end


end
%Get RT (how long took to answer)
RT = time;                                          % converting RT to second
% % Clear the screen
% Screen('Flip', window);
end