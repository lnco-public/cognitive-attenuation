---------Cognitive attenuation task---------
---------Written by Giedre Stripeikyte 2018------
20 trials per condition
3 runs with 4,3,3 blocks respectively
One block has 4 trials of the same condition

Task layout:
-  Instructions (4 s)
-  Word generation (active condition) and listening (passive condition)
-  Numerosity report (7 s)
-  Error report	(7 s)

NaN values are reported in .mat files as 999

---------------------------------------------------
The main script is in folder MRI_setup: 'MRI_TI_task_setup_3runs_instscreen_cf.m'

The folder 'Training' contains short version of the task of 4 trials with specific selected letters 
aimed for participants to get used to the task. Usualy performed in the mock scanner.

---------------------------------------------------
N.B. 

The script will listen to these key buttons 
(change the wanted buttons in the scritp)

From Line 196 in the matlab main script

ENTER = KbName('return');
Left = KbName('2@');
Right = KbName('3#');
Confirm = KbName('1!');

The script also waits for the trigger which is usually given by MRI machine
or can do manual input KbName('5%');
The trigger input if not run in MRI should be removed for the conveniece since do not need to sync
with the scanning time

If the script crashes or is quit before the experiment is finished,
 need to restart the matlab because the keyboard will be disabled

--------------------------------------------------
All scripts and functions should be in their appropriate folders. 

If change the directories please take care that it is changed in the scripts

The folders should be in one directiory:
'Audio', 'Task'
Do not move scripts from the 'Task' folder