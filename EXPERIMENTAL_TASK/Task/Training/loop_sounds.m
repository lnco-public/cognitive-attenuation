function [key_press_other] = loop_sounds(pahandle, Confirm, gender, trial, word_in, workdir, times_between)


%-----------------------------------------------------------------
%	The inputs needed if want to try function (examples)
%-----------------------------------------------------------------
% workdir = 'E:\TI_MRI_setup\';
% times_between = [2.1365, 1.3199, 0.8721, 0.7840, 2.8800];
% word_in = [5 255 16 18 3]; 
% word_indx = [0 0 word_in];
% letter_indx = 5;
%-----------------------------------------------------------------
%	several sounds playing
%----------------------------------------------------------------- 

word_indx = [0 0 word_in']; %the needed one
played_times = [2 2 times_between 2]; 
if gender == 'm' 
    sex = 'male';
else 
    sex= 'female';
end    
    


    for i=3:length(word_indx)
        wavfilenames{i} = [ workdir 'Audio\audio_' gender '\' sex num2str(word_indx(i)) '.wav' ];
    end 
wavfilenames{1} = [workdir 'Audio\ecoute_' gender '.wav']; 
wavfilenames{2} = [workdir 'Audio\training_letter_' gender '_' num2str(trial) '.wav']; 
wavfilenames{(length(word_indx)+1)}= [workdir 'Audio\BEEP.wav'];
nfiles = length(wavfilenames);

% Always init to 2 channels, for the sake of simplicity:
nrchannels = 2;

% Does a function for resampling exist?
if exist('resample') %#ok<EXIST>
    % Yes: Select a target sampling rate of 44100 Hz, resample if
    % neccessary:
    freq = 44100;
    doresample = 1;
else
    % No. We will choose the frequency of the wav file with the highest
    % frequency for actual playback. Wav files with deviating frequencies
    % will play too fast or too slow, b'cause we can't resample:
    % Init freq:
    freq = 0;
    doresample = 0;
end

% Read all sound files and create & fill one dynamic audiobuffer for
% each read soundfile:
buffer = [];
j = 0;

for i=1:nfiles
    try
        % Make sure we don't abort if we encounter an unreadable sound
        % file. This is achieved by the try-catch clauses...
        [audiodata, infreq] = psychwavread(char(wavfilenames(i)));
        dontskip = 1;
    catch
        fprintf('Failed to read and add file %s. Skipped.\n', char(wavfilenames(i)));
        dontskip = 0;
        psychlasterror
        psychlasterror('reset');
    end

    if dontskip
        j = j + 1;

        if doresample
            % Resampling supported. Check if needed:
            if infreq ~= freq
                % Need to resample this to target frequency 'freq':
                fprintf('Resampling from %i Hz to %i Hz... ', infreq, freq);
                audiodata = resample(audiodata, freq, infreq);
            end
        else
            % Resampling not supported by Matlab/Octave version:
            % Adapt final playout frequency to maximum frequency found, and
            % hope that all files match...
            freq = max(infreq, freq);
        end

        [samplecount, ninchannels] = size(audiodata);
        audiodata = repmat(transpose(audiodata), nrchannels / ninchannels, 1);

        buffer(end+1) = PsychPortAudio('CreateBuffer', [], audiodata); %#ok<AGROW>
        [fpath, fname] = fileparts(char(wavfilenames(j)));
        fprintf('Filling audiobuffer handle %i with soundfile %s ...\n', buffer(j), fname);
    end
end 

% Recompute number of available sounds:
nfiles = length(buffer);

suggestedLatencySecs = [];

if IsARM
    % ARM processor, probably the RaspberryPi SoC. This can not quite handle the
    % low latency settings of a Intel PC, so be more lenient:
    suggestedLatencySecs = 0.001;
    fprintf('Choosing a high suggestedLatencySecs setting of 25 msecs to account for lower performing ARM SoC.\n');
end

% Open the default audio device [], with default mode [] (==Only playback),
% and a required latencyclass of 1 == standard low-latency mode, as well as
% a playback frequency of 'freq' and 'nrchannels' sound output channels.
% This returns a handle 'pahandle' to the audio device:
%pahandle = PsychPortAudio('Open', [], [], 1, freq, nrchannels, [], suggestedLatencySecs);

% Enable use of sound schedules: We create a schedule of default size,
% currently 128 slots by default. From now on, the driver will not play
% back the sounds stored via PsychPortAudio('FillBuffer') anymore. Instead
% you'll have to define a "playlist" or schedule via subsequent calls to
% PsychPortAudio('AddToSchedule'). Then the driver will process that
% schedule by playing all defined sounds in the schedule, one after each
% other, until the end of the schedule is reached. You can add new items to
% the schedule while the schedule is already playing.
PsychPortAudio('UseSchedule', pahandle, 1);

% Build an initial play sequence. Play each buffer once for a starter:
Total_times = [];
for i=1:nfiles
    %played time selection and constrictions, fitting in range
    time = played_times(i);
    if time < 2 
        time = 2;
    elseif time > 4 
        time = 4; 
    elseif i == nfiles 
        time = 0.5;
    end 
    
    % Play buffer(i) from startSample 0.0 seconds to endSample 4.0 
    % seconds. Play one repetition of each soundbuffer...
    PsychPortAudio('AddToSchedule', pahandle, buffer(i), 1, 0.0, time, 1); 
    Total_times(i)= time;
end 

S = sum(Total_times);
PsychPortAudio('Start', pahandle, [], 0, 1); 
%WaitSecs(3*length(wavfilenames));
key_press_other = 0;
startCounting = GetSecs();
elapsedTime = GetSecs() - startCounting;
while elapsedTime < S %3*(nfiles-1.5)
    [keyIsDown, seconds, keyCode] = KbCheck;  
    if keyCode(Confirm) == 1
        key_press_other = key_press_other + 1;
        disp(key_press_other)
        KbReleaseWait;

    end
    elapsedTime = GetSecs() - startCounting;
end

% Stop playback: Stop immediately, but wait for stop to happen:
PsychPortAudio('Stop', pahandle, 1, 1); 
PsychPortAudio('DeleteBuffer');

end