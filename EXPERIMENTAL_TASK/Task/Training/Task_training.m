% Word numerosity judgement task, to run in MRI 
%Giedre 2017-2018  


% *******TASK DESCRIPTION******* 
%**********TRAINING***********
% The task has two main conditions: self
% (subject generates silently as many words as possible in limited amout of
% time starting from the given cue letter. For each generated word press the
% button) and other (subject listens to the prerecorded list of words and
% have to notice the cued letter in the word. At the end of each trial (self
% or other)subject has to judge the number of words generated/heard. Before
% each trial subject hear short instruction (Ecoute or Pense). The task is
% performed with eyes closed, only when hear "BEEP" open the eyes. Task is
% balanced depending on the performance: depending on how many words were
% generated in 'self' condition, the same number of words will be played in
% 'other' condition. If no word were generated during 'self' condition 8
% word list will be played. Words are selected randomly but balancing for
% the cue letters in word. Cue letters are selected randomly but the same
% used for 'self' and 'other' conditions (in shuffled order) The break is
% inctroduced after 4 repetitions
%Waiting seconds for judgement and confidence 7s

% Clear the workspace
clearvars;
clc; 
clear all;  
sca;
rng shuffle;


%working directory
workdir = 'E:\TI_MRI_setup\';

%-------------------------
%	Set up subject number
%-------------------------

    gender = input('Enter Subject Gender (always put f): ','s');
        
%Variables needed for the task
rest = 4; %s (should be 5s)
%repetitions = 4; % repetitions of each condition
words_heard = [];
total_time = 7;

%-----------------------------------------------------------------
%	Generate letter list, which will be used for self and other conditions 
%-----------------------------------------------------------------

load([workdir 'Task\Training\words.mat']) %contains all words and letters and their indexes corresponding to file naming

%-----------------------------------------------------------------
%	Audio settings 
%-----------------------------------------------------------------

% Number of channels and Frequency of the sound
nrchannels = 2;
freq = 44100;
Beep = psychwavread([workdir 'Audio\BEEP.wav']);
Beep = [Beep Beep];
Pense = psychwavread([workdir 'Audio\panse_' gender '.wav']);
InitializePsychSound;
% Try with the 'freq'uency we wanted:
pahandle = PsychPortAudio('Open', [], [], 0, freq, nrchannels, []);  
%-----------------------------------------------------------------
%	Screen open 
%-----------------------------------------------------------------

% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);
% Get the screen numbers
screens = Screen('Screens');
% Draw to the external screen if avaliable
screenNumber = max(screens);
% Define black and white
white = WhiteIndex(screenNumber);
black = BlackIndex(screenNumber);
grey = white/2;
% Open an on screen window
[window, windowRect] = PsychImaging('OpenWindow', screenNumber, black); 
% Set the blend funciton for the screen
Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
% Sync us and get a time stamp
vbl = Screen('Flip', window);
waitframes = 1;
Screen('Flip',window); %swaps backbuffer to frontbuffer 
Screen('TextSize', window, 30);
% Get the size of the on screen window
[screenXpixels, screenYpixels] = Screen('WindowSize', window); 
% Query the frame duration
ifi = Screen('GetFlipInterval', window);
% Get the centre coordinate of the window
[xCenter, yCenter] = RectCenter(windowRect);
% Maximum priority level
topPriorityLevel = MaxPriority(window);
Priority(topPriorityLevel);
instruction = 'Fermez les yeux jusqu''� entendre un bip';

%Keys settings for key presses 
KbName('UnifyKeyNames');
%upArrow = KbName('UpArrow'); 
ENTER = KbName('return');
Left = KbName('2@');
Right = KbName('3#');
Confirm = KbName('1!');
    
    %-----------------------------------------------------------------
    %	Press the button to start the experiment 
    %-----------------------------------------------------------------

    disp('********************** Press a ENTER to start! ***********************' ) 
    RestrictKeysForKbCheck([ENTER]);
    KbStrokeWait; 
    RestrictKeysForKbCheck([]);
    triggerTime_run = GetSecs;

    for trial = 1:2
            %-----------------------------------------------------------------
            %	SELF
            %-----------------------------------------------------------------
            disp ('current selected condition is: SELF')
            
            t = 5;


                %-----------------------------------------------------------------
                %	Pause with close eyes instruction
                %-----------------------------------------------------------------
                
                DrawFormattedText(window, [instruction],'center', 'center', grey);
                Screen('Flip', window);
                WaitSecs(rest)     %introduce pause (5s) 
                Screen('Flip', window); 
                WaitSecs(10);
                %-----------------------------------------------------------------
                 %	one sound playing
                %-----------------------------------------------------------------
                % Read WAV letter file from filesystem:
                Letter = psychwavread ([workdir 'Audio\training_letter_' gender '_' num2str(trial) '.wav']);

                PsychPortAudio('UseSchedule', pahandle, 0);
                PsychPortAudio('FillBuffer', pahandle, Pense'); 
                PsychPortAudio('Start', pahandle, 1, 0, 1);   
                PsychPortAudio('FillBuffer', pahandle, Letter'); 
                PsychPortAudio('Start', pahandle, 1, 0, 1);  

                %Count key presses while waiting for the beep and generating
                %words 

                % Set up the timer
                startTime = now;
                durationInSeconds = (t+1.5);
                numberOfSecondsRemaining = durationInSeconds;
                key_press_self = 0;
                time_press = []; 
                time_between_words = [];
                while numberOfSecondsRemaining > 0 
                    numberOfSecondsElapsed = round((now - startTime) * 10 ^ 5);
                    numberOfSecondsRemaining = durationInSeconds - numberOfSecondsElapsed;
                    [ keyIsDown, seconds, keyCode ] = KbCheck;  
                    if keyCode(Confirm) == 1
                        key_press_self = key_press_self + 1; 
                        KbReleaseWait; 
                        disp (key_press_self)
                    end

                end
                %when generation has to end play beep
                PsychPortAudio('FillBuffer', pahandle, Beep'); 
                PsychPortAudio('Start', pahandle, 1, 0, 1);
                % Stop playback:
                PsychPortAudio('Stop', pahandle, 0, 1);
                PsychPortAudio('DeleteBuffer');
                WaitSecs(1);
                %***************to do judgement***************
                selected_cond = 1;
                [press_time, press, number, number_primary, RT] = numerosity_judgement(triggerTime_run, waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, Left, Right, total_time, selected_cond, window, windowRect, grey); %need to change just flip screen, not to close
                WaitSecs(total_time - RT);
                % Clear the screen
                Screen('Flip', window);
                WaitSecs(1);
                %***************confidence*************** 
                if number < 999 %999 is used as missed value to answer
                    [conf, RT_conf] = error_eval_mov(waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, total_time, number, window, windowRect, grey);
                    WaitSecs(total_time - RT_conf);
                    % Clear the screen
                    Screen('Flip', window);
                else 
                    disp('Skipping confidence judgement')
                    conf = 999;
                    RT_conf = 999;
                    WaitSecs(total_time);
                end    

                %Accuracy 
                acc = number - key_press_self;

            %-----------------------------------------------------------------
            %	OTHER
            %-----------------------------------------------------------------
            disp ('current selected condition is: OTHER')
            %the order for the word range selection and times, schuffled for
            %each mini block

                %-----------------------------------------------------------------
                %	Pause with close eyes instruction
                %-----------------------------------------------------------------
                DrawFormattedText(window, [instruction],'center', 'center', grey);
                Screen('Flip', window);
                WaitSecs(rest/2)     %introduce pause (5s) 
                Screen('Flip', window);
                WaitSecs(rest/2) 

                times_between = randi([2,4],1,9);   
                range = 9;
               if trial == 1
                    letter_indx = 9;                    
               else
                    letter{22,1} = 'k';    
                    letter_indx = 21;
               end 
               %function which generates list of words to be played including
               %controled amount of cued words
               [word_in, selected_cues] = word_list(range, letter_indx, letter, word, index_w);

               %listen to the pre-recorded sound
               [key_press_other] = loop_sounds(pahandle, Confirm, gender, trial, word_in, workdir, times_between);  %call function to play selected sounds (including instruction, cue letter and beep at the end
               WaitSecs(1);
               %***************to do judgement*************** 
               selected_cond = 2;
               [press_time, press, number, number_primary, RT] = numerosity_judgement(triggerTime_run, waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, Left, Right, total_time, selected_cond, window, windowRect, grey);%need to change just flip screen, not to close
               WaitSecs(total_time - RT);
               % Clear the screen
               Screen('Flip', window);
               WaitSecs(1);
             
               %***************confidence*************** 
               if number < 999
                    [conf, RT_conf] = error_eval_mov(waitframes, xCenter, yCenter, ifi, screenXpixels, screenYpixels, Confirm, total_time, number, window, windowRect, grey);
                    WaitSecs(total_time - RT_conf);
                    % Clear the screen
                    Screen('Flip', window);
               else 
                    disp('Skipping confidence judgement') 
                    conf = 999;
                    RT_conf = 999;
                    WaitSecs(total_time);
               end

               %Accuracy 
               acc = number - range;
               PressTime{trial,1} = press_time; 
               PressJudg{trial,1} = press;

    end
               DrawFormattedText(window, 'Training done!','center', 'center', grey);
               Screen('Flip', window);
               % Close the audio device:
               PsychPortAudio('Close', pahandle);
               save('Judgement_press.mat','PressJudg', 'PressTime')
               WaitSecs(10);
               sca
